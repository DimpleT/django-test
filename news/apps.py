from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'news'
    varbose_name = 'News'
