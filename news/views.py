from django.views.generic import ListView, DetailView, CreateView
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.contrib.auth import authenticate, login, logout

from .models import News, Category
from .form import AddNewsForm, UserRegisterForm, UserLoginForm
from django.contrib import messages


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, 'You are success register')
            return redirect('news:home')
        else:
            messages.error(request, 'Error register')
    else:
        form = UserRegisterForm()
    return render(request, 'news/register.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            messages.success(request, 'You are success login')
            return redirect('news:home')
        else:
            messages.error(request, 'Error login')
    else:
        form = UserLoginForm()
    return render(request, 'news/login.html', {'form': form})


def user_logout(request):
    logout(request)
    return redirect('news:login')


class HomeNews(ListView):
    model = News
    template_name = 'news/news_list.html'
    context_object_name = 'news'
    paginate_by = 1

    # extra_context = {
    #     'title': 'All News'
    # }

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'All News'
        return context

    def get_queryset(self):
        return News.objects.filter(is_published=True).order_by('-created_at').select_related('category_id')


class NewsByCategory(ListView):
    model = News
    template_name = 'news/category.html'
    context_object_name = 'news'
    allow_empty = False

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['curCategory'] = Category.objects.get(slug=self.kwargs['category_slug'])
        return context

    def get_queryset(self):
        category = Category.objects.get(slug=self.kwargs['category_slug'])
        return News.objects.filter(category_id=category.pk, is_published=True).select_related('category_id')


class ViewNews(DetailView):
    model = News
    slug_url_kwarg = 'news_slug'
    template_name = 'news/view_news.html'
    context_object_name = 'newsItem'


class CreateNews(LoginRequiredMixin, CreateView):
    login_url = '/admin/'
    form_class = AddNewsForm
    template_name = 'news/add_news.html'
    # success_url = reverse_lazy('news:home')


'''
def index(request):
    news = News.objects.filter(is_published=True).order_by('-created_at')
    paginator = Paginator(news, 2)
    page = request.GET.get('page', 1)
    news_obj = paginator.get_page(page)
    return render(request, 'news/index.html', {
        'news': news_obj,
        'title': 'All News'
    })
'''

'''
def get_category(request, category_slug):
    category = Category.objects.get(slug=category_slug)
    news = News.objects.filter(category_id=category.pk, is_published=True)
    return render(request, 'news/category.html', {
        'news': news,
        'curCategory': category
    })
'''

'''
def view_news(request, news_slug):
    newsItem = get_object_or_404(News, slug=news_slug)
    return render(request, 'news/view_news.html', {'newsItem': newsItem})
'''


'''
def add_news(request):
    if request.method == 'POST':
        form = AddNewsForm(request.POST)
        if form.is_valid():
            # news = News.objects.create(**form.cleaned_data)
            news = form.save()
            return redirect(news)
    else:
        form = AddNewsForm()
    return render(request, 'news/add_news.html', {'form': form})
'''